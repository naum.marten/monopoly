from time import time

from adj.adjudicator import Adjudicator
from adj.gui import Gui
from agents.fixedagent import FixedAgent

gui = Gui(turnLength=0)

while True:
    adjudicator = Adjudicator()
    agent1 = FixedAgent(1, 300, 150)
    agent2 = FixedAgent(2, 300, 150)
    start = time()
    winner, state = adjudicator.runGame(agent1, agent2, gui=gui)
    end = time()
    print(winner, state)
    print(end - start)
    try: input("Press enter to play again")
    except SyntaxError: pass
