import sys

sys.path.append('../')

from multiprocessing import Pool as ThreadPool
from adj.state import State
from adjudicator import Adjudicator
from agents.fixedagent import FixedAgent
from agents.riskyagent import RiskyAgent
import matplotlib.pyplot as plt

NUM_CORES = 4

def runGame(params):
    minMoney = params
    adjudicator = Adjudicator()
    agent1 = RiskyAgent(1, minMoney)
    agent2 = FixedAgent(2, 300, 200)
    return adjudicator.runGame(agent1, agent2)

def runNGames(n, params):
    pool = ThreadPool(NUM_CORES)
    results = pool.map(runGame, [params] * n)
    wins = [0, 0]
    timeouts = 0
    for result in results:
        winner, state = result
        wins[winner - 1] += 1
        if State(state).turn == 100: timeouts += 1
    return wins, timeouts

if __name__ == '__main__':
    numGames = 1000
    minMoney = 10
    results = []
    oldWinRate = 50
    alpha = 5

    while(minMoney < 600):
        wins, timeouts = runNGames(numGames, minMoney)

        print("Player 1 won %d times" % wins[0])
        print("Player 2 won %d times" % wins[1])
        print("P1 win ratio: " + str(wins[0] / (numGames * 1.0)))
        print("Timeout ratio: " + str(timeouts / (numGames * 1.0)))
        results.append((minMoney,wins,timeouts))
        print("MinMoney: %d\n" % minMoney) 

        minMoney+=20
        #winRate = int((wins[0] / float(numGames)) * 100)
        #minMoney += alpha * (winRate - oldWinRate)
        #oldWinRate = winRate

    minMonies = [tup[0] for tup in results]
    winRates = [tup[1][0] / float(numGames) for tup in results]
    timeoutRates = [tup[2] / float(numGames) for tup in results]

    plt.plot(minMonies, winRates)
    plt.xlabel('Minimum Amount of Money to Save')
    plt.ylabel('Win Rate')
    plt.show()

    plt.plot(minMonies, timeoutRates)
    plt.xlabel('Minimum Amount of Money to Save')
    plt.ylabel('Timeout Rate')
    plt.show()
