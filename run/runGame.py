import sys

sys.path.append('../')
from agents.riskyagent import RiskyAgent
from agents.fixedagent import FixedAgent
from adj.state import State
from adjudicator import Adjudicator

playerWins = [0, 0]
timeouts = 0
numGames = 1000

for i in range(0, numGames):
    adjudicator = Adjudicator()
    agent1 = RiskyAgent(1)
    agent2 = FixedAgent(2, 300, 200)
    winner, state = adjudicator.runGame(agent1, agent2)
    playerWins[winner - 1] += 1
    if State(state).turn == 100: timeouts += 1

print("Player 1 won %d times" % playerWins[0])
print("Player 2 won %d times" % playerWins[1])
print("P1 win ratio: " + str(playerWins[0] / (numGames * 1.0)))
print("Timeout ratio: " + str(timeouts / (numGames * 1.0)))
