import sys
sys.path.append('../')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns

from adj.adjudicator import Adjudicator
from agents.fixedagent import FixedAgent
from agents.randomagent import RandomAgent

adjudicator = Adjudicator()
agent1 = FixedAgent(0, 300, 150)
agent2 = RandomAgent(1)

playerWins = [0, 0]

maxThresholds = [300, 400, 500, 600]
minThresholds = [50, 100, 150, 200]
winRates = []
numRuns = 1000
for maxThreshold in maxThresholds:
    for minThreshold in minThresholds:
        agent1 = FixedAgent(0, maxThreshold, minThreshold)
        for i in range(0, numRuns):
            winner, state = adjudicator.runGame(agent1, agent2)
            playerWins[winner - 1] += 1
        winRates.append(playerWins[0] / float(numRuns))

        print("MaxThreshold: %d, MinThreshold: %d, WinRate: %.2f" % (maxThreshold, minThreshold, playerWins[0] / float(numRuns)))
        playerWins = [0, 0]

maxThresholds = np.asarray(maxThresholds)
minThresholds = np.asarray(minThresholds)
winRates = np.asarray(winRates)
maxThresholds = np.repeat(maxThresholds, 4)
minThresholds = np.tile(minThresholds, 4)
data = pd.DataFrame(data={'maxThresholds': maxThresholds, 'minThresholds': minThresholds, 'winRates': winRates})
data = data.pivot(index='maxThresholds', columns='minThresholds', values='winRates')
print(data)
ax = sns.heatmap(data, cmap="Blues", linewidth=0.5)
plt.show()

'''
for i in range(0,100):
	winner, state = adjudicator.runGame(agent1,agent2)
	playerWins[winner-1] += 1

print("Player 1 won %d times" % playerWins[0])
print("Player 2 won %d times" % playerWins[1])
print(playerWins[0] / 100.0)
print(state)
'''
