import sys

sys.path.append('../')
from adj.gui import Gui
from adj.state import State
from adjudicator import Adjudicator
from agents.riskyagent import RiskyAgent

while True:
    adjudicator = Adjudicator()
    agent1 = RiskyAgent(1)
    agent2 = RiskyAgent(2)
    winner, state = adjudicator.runGame(agent1, agent2)
    state = State(state)
    print("Winner: ", winner)
    print("State: ", state)
    gui = Gui(states=state.pastStates, turnLength=0.02)
    try: input("Press enter to play again")
    except SyntaxError: pass
