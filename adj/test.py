from collections import namedtuple

StateTuple = namedtuple("StateTuple", "turn properties positions money phase phaseData debt pastStates")

class TestAgent:
    def __init__(self, id):
        self.id = id

    def getBSMTDecision(self, state):
        return None

    def respondTrade(self, state):
        return False

    def buyProperty(self, state):
        return True

    def auctionProperty(self, state):
        return 0

    def jailDecision(self, state):
        return "R"

    def receiveState(self, state):
        pass

def testChanceCards(adjudicator):
    agent1 = TestAgent(1)
    agent2 = TestAgent(2)
    result = True
    for card in range(0, 16):
        winner, state = adjudicator.runGame(agent1, agent2, [(3, 4)], [card], [])
        state = StateTuple(*state)
        conditions = [
            state.positions == (0, 0) and state.money == (1700, 1500),
            state.positions == (24, 0) and state.properties[24] == 1 and state.money == (1260, 1500),
            state.positions == (11, 0) and state.properties[11] == 1 and state.money == (1360, 1500),
            state.positions == (12, 0) and state.properties[12] == 1 and state.money == (1350, 1500),
            state.positions == (15, 0) and state.properties[15] == 1 and state.money == (1300, 1500),
            state.positions == (15, 0) and state.properties[15] == 1 and state.money == (1300, 1500),
            state.positions == (7, 0) and state.money == (1550, 1500),
            state.positions == (7, 0) and state.properties[40] == 1 and state.money == (1500, 1500),
            state.positions == (4, 0) and state.money == (1300, 1500),
            state.positions == (-1, 0) and state.money == (1500, 1500),
            state.positions == (7, 0) and state.money == (1500, 1500),
            state.positions == (7, 0) and state.money == (1485, 1500),
            state.positions == (5, 0) and state.properties[5] == 1 and state.money == (1500, 1500),
            state.positions == (39, 0) and state.properties[39] == 1 and state.money == (1100, 1500),
            state.positions == (7, 0) and state.money == (1450, 1550),
            state.positions == (7, 0) and state.money == (1650, 1500)
        ]
        if not conditions[card]:
            print("Test failed on chance card " + str(card))
            result = False
    return result

def testCommunityCards(adjudicator):
    agent1 = TestAgent(1)
    agent2 = TestAgent(2)
    result = True
    for card in range(0, 17):
        winner, state = adjudicator.runGame(agent1, agent2, [(1, 1)], [], [card])
        state = StateTuple(*state)
        conditions = [
            state.positions == (0, 0) and state.money == (1700, 1500),
            state.positions == (2, 0) and state.money == (1700, 1500),
            state.positions == (2, 0) and state.money == (1450, 1500),
            state.positions == (2, 0) and state.money == (1550, 1500),
            state.positions == (2, 0) and state.properties[41] == 1 and state.money == (1500, 1500),
            state.positions == (-1, 0) and state.money == (1500, 1500),
            state.positions == (2, 0) and state.money == (1550, 1450),
            state.positions == (2, 0) and state.money == (1600, 1500),
            state.positions == (2, 0) and state.money == (1520, 1500),
            state.positions == (2, 0) and state.money == (1510, 1490),
            state.positions == (2, 0) and state.money == (1600, 1500),
            state.positions == (2, 0) and state.money == (1450, 1500),
            state.positions == (2, 0) and state.money == (1450, 1500),
            state.positions == (2, 0) and state.money == (1525, 1500),
            state.positions == (2, 0) and state.money == (1500, 1500),
            state.positions == (2, 0) and state.money == (1510, 1500),
            state.positions == (2, 0) and state.money == (1600, 1500)
        ]
        if not conditions[card]:
            print("Test failed on community chest card " + str(card))
            result = False
    return result

def testChanceCardRents(adjudicator):
    agent1 = TestAgent(1)
    agent2 = TestAgent(2)
    result = True
    for card in [3, 4, 5]:
        rolls = [(6, 6), (1, 2), (3, 4)]
        # add roll for utility rent calculation (should produce rent of 10 * 10 = 100)
        if card == 3: rolls.append((5, 5))
        winner, state = adjudicator.runGame(agent1, agent2, rolls, [card], [])
        state = StateTuple(*state)
        # check that player 1 bought electric company and pennsylvania railroad
        if not (state.properties[12] == 1 and state.properties[15] == 1): return False
        if card == 3 and not (state.positions == (15, 12) and state.money == (1250, 1400)):
            print("Test failed on rent for chance card " + str(card))
            result = False
        if (card == 4 or card == 5) and not (state.positions == (15, 15) and state.money == (1200, 1450)):
            print("Test failed on rent for chance card " + str(card))
            result = False
    return result

def testRailroadRents(adjudicator):
    agent1 = TestAgent(1)
    agent2 = TestAgent(2)
    rolls = [(1, 4), (1, 4), (4, 6), (4, 6), (4, 6), (4, 6), (4, 6), (4, 6)]
    winner, state = adjudicator.runGame(agent1, agent2, rolls, [], [])
    state = StateTuple(*state)
    # check that player 1 bought all the railroads and that the rents were correct
    return state.properties[5] == 1 and state.properties[15] == 1 and \
           state.properties[25] == 1 and state.properties[35] == 1 and \
           state.positions == (35, 35) and \
           state.money == (1500 - 800 + 25 + 50 + 100 + 200, 1500 - 25 - 50 - 100 - 200)

def testUtilityRents(adjudicator):
    agent1 = TestAgent(1)
    agent2 = TestAgent(2)
    rolls = [(6, 6), (3, 5), (6, 6), (3, 5), (3, 5), (3, 5)]
    winner, state = adjudicator.runGame(agent1, agent2, rolls, [], [])
    state = StateTuple(*state)
    # check that player 1 bought both utilites and that the rents were correct
    return state.properties[12] == 1 and state.properties[28] == 1 and \
           state.positions == (28, 28) and \
           state.money == (1500 - 300 + 12 * 4 + 8 * 10, 1500 - 12 * 4 - 8 * 10)

def testTaxes(adjudicator):
    agent1 = TestAgent(1)
    agent2 = TestAgent(2)
    rolls = [(5, 5), (5, 5), (5, 6), (1, 3), (3, 4)]
    winner, state = adjudicator.runGame(agent1, agent2, rolls, [], [])
    state = StateTuple(*state)
    # check that player 1 bought pacific avenue (side effect of getting to luxury tax)
    # and that the taxes were correct
    return state.properties[31] == 1 and \
           state.positions == (38, 4) and \
           state.money == (1500 - 300 - 100, 1500 - 200)

tests = [
    testChanceCards,
    testCommunityCards,
    testChanceCardRents,
    testRailroadRents,
    testUtilityRents,
    testTaxes
]

def runTests(adjudicator):
    allPassed = True
    for test in tests:
        result = test(adjudicator)
        if not result:
            print(test.__name__ + " failed!")
            allPassed = False
    if allPassed: print("All tests passed!")
