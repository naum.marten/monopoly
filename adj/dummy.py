class Dummy:
    def __init__(self, id):
        self.id = id
        pass

    def getBSMTDecision(self, state):
        return None

    def respondTrade(self, state):
        return False

    def buyProperty(self, state):
        return False

    def auctionProperty(self, state):
        return 0

    def jailDecision(self, state):
        return "R"

    def receiveState(self, state):
        pass
