from adj.state import State
from .board import Type

try: import tkinter as tk
except ImportError: import Tkinter as tk

class Gui:
    def __init__(self, states, turnLength=0.01):
        self.states = list(states)
        self.turnLength = turnLength
        self.root = tk.Tk()
        self.boardImage = tk.PhotoImage(file=__file__ + "/../img/board.gif")
        boardWidth = self.boardImage.width()
        boardHeight = self.boardImage.height()
        padding = 50
        self.canvas = tk.Canvas(self.root, width=boardWidth + padding * 2, height=boardHeight + padding * 2)
        self.canvas.pack(side="left")
        self.canvas.create_image(padding, padding, image=self.boardImage, anchor="nw")
        self.rects = []
        for id in range(0, 42):
            x = y = w = h = x1 = y1 = 0
            if id < 10:
                x = 701 - (id % 10) * 61.5
                y = 800
                w = 61.5
                h = 20
                x1 = x
                y1 = y - 97
            elif id < 20:
                x = 30
                y = 701 - (id % 10) * 61.5
                w = 20
                h = 61.5
                x1 = x + 97
                y1 = y
            elif id < 30:
                x = 86 + (id % 10) * 61.5
                y = 30
                w = 61.5
                h = 20
                x1 = x
                y1 = y + 97
            elif id < 40:
                x = 800
                y = 86 + (id % 10) * 61.5
                w = 20
                h = 61.5
                x1 = x - 97
                y1 = y
            dx = w / 4 if w > h else 0
            dy = h / 4 if h > w else 0
            w1 = w / 4 if w > h else w
            h1 = h / 4 if h > w else h
            w2 = w / 2 if w > h else w
            h2 = h / 2 if h > w else h
            self.rects.append([
                self.canvas.create_rectangle(x, y, x + w, y + h, fill="", outline=""),
                self.canvas.create_rectangle(x1 + dx * 0, y1 + dy * 0, x1 + w1 + dx * 0, y1 + h1 + dy * 0, fill="", outline=""),
                self.canvas.create_rectangle(x1 + dx * 1, y1 + dy * 1, x1 + w1 + dx * 1, y1 + h1 + dy * 1, fill="", outline=""),
                self.canvas.create_rectangle(x1 + dx * 2, y1 + dy * 2, x1 + w1 + dx * 2, y1 + h1 + dy * 2, fill="", outline=""),
                self.canvas.create_rectangle(x1 + dx * 3, y1 + dy * 3, x1 + w1 + dx * 3, y1 + h1 + dy * 3, fill="", outline=""),
                self.canvas.create_rectangle(x1 + dx * 1, y1 + dy * 1, x1 + w2 + dx * 1, y1 + h2 + dy * 1, fill="", outline="")
            ])
        self.playerRects = [
            self.canvas.create_rectangle(0, 0, 0, 0, fill="blue", outline="black"),
            self.canvas.create_rectangle(0, 0, 0, 0, fill="red", outline="black")
        ]
        self.player1CashText = self.canvas.create_text(160, 620, anchor="nw")
        self.player2CashText = self.canvas.create_text(160, 640, anchor="nw")
        self.turnText = self.canvas.create_text(160, 660, anchor="nw")
        self.root.after(0, self.loop)
        self.root.mainloop()

    def reset(self):
        for rectList in self.rects:
            for rect in rectList:
                self.canvas.itemconfig(rect, fill="", outline="")
        for rect in self.playerRects:
            self.canvas.coords(rect, 0, 0, 0, 0)

    def loop(self):
        if not self.states: return
        self.drawState(self.states.pop(0))
        self.root.after(int(self.turnLength * 1000), self.loop)

    def drawState(self, pastState):
        pastState[1].append([])
        state = State(pastState[1])
        for prop in state.properties:
            if prop.owner == -1 or prop.id >= 40: continue
            ownerRect = self.rects[prop.id][0]
            fill = "#0000ff" if prop.owner == 0 else "#ff0000"
            if state.playerOwnsGroup(prop.owner, prop.data.group):
                fill = "#8888ff" if prop.owner == 0 else "#ff8888"
            if prop.mortgaged:
                fill = "#000088" if prop.owner == 0 else "#880000"
            self.canvas.itemconfig(ownerRect, fill=fill, outline="black")
            if prop.data.type != Type.PROPERTY: continue
            for i in range(1, 6):
                rect = self.rects[prop.id][i]
                if prop.houses == 5:
                    if i == 5: self.canvas.itemconfig(rect, fill="red", outline="black")
                    else: self.canvas.itemconfig(rect, fill="", outline="")
                else:
                    if prop.houses >= i: self.canvas.itemconfig(rect, fill="green", outline="black")
                    else: self.canvas.itemconfig(rect, fill="", outline="")
        for player, position in enumerate(state.positions):
            rect = self.playerRects[player]
            if position == -1:
                x = 100
                y = 725
            else:
                x1, y1, _, _ = self.canvas.coords(self.rects[position][0])
                x2, y2, _, _ = self.canvas.coords(self.rects[position][1 + player * 3])
                x = (x1 * 2 + x2) / 3
                y = (y1 * 2 + y2) / 3
            self.canvas.coords(rect, x, y, x + 20, y + 20)
        self.canvas.itemconfig(self.player1CashText, text="P1: $" + str(state.money[0]))
        self.canvas.itemconfig(self.player2CashText, text="P2: $" + str(state.money[1]))
        self.canvas.itemconfig(self.turnText, text="Turn: " + str(state.turn))
