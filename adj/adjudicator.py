from .board import Type, Group, CHANCE_GOJF_PROPERTY, COMMUNITY_GOJF_PROPERTY, CHANCE_GOJF_CARD, COMMUNITY_GOJF_CARD, MAX_HOUSES, MAX_HOTELS
from .state import State, Phase, TradeData, GameOverException

class Adjudicator:
    def __init__(self):
        pass

    def runGame(self, player1, player2, diceRolls=None, chanceCards=None, communityCards=None, gui=None):
        self.state = State()
        self.players = [0, 1]
        self.agents = [player1, player2]
        self.gui = gui
        if self.gui: self.gui.reset()
        if diceRolls is not None:
            self.state.diceRolls = diceRolls
        if chanceCards is not None:
            self.state.chanceCards = chanceCards
        if communityCards is not None:
            self.state.communityCards = communityCards
        try:
            while self.state.turn < 100:
                self.__runTurn()
            return self.__decideWinner(), self.state.toTuple()
        except GameOverException:
            return self.state.winner + 1, self.state.toTuple()

    def __runPlayer(self, player):
        return self.runPlayerOnState(player, self.stateTuple)

    def runPlayerOnState(self, player, state):
        agent = self.agents[player]
        if state.phase == Phase.BSMT:
            return agent.getBSMTDecision(state)
        elif state.phase == Phase.TRADE:
            return agent.respondTrade(state)
        elif state.phase == Phase.BUY:
            return agent.buyProperty(state)
        elif state.phase == Phase.AUCTION:
            return agent.auctionProperty(state)
        elif state.phase == Phase.JAIL:
            return agent.jailDecision(state)

    def __runTurn(self):
        self.__runBSMT()
        player = self.players[self.state.turn % 2]
        roll = self.state.getNextRoll()
        # in jail
        if self.state.positions[player] == -1:
            jailed = True
            self.state.phase = Phase.JAIL
            self.state.jailTurns[player] += 1
            self.__commitState()
            action = self.__runPlayer(player)
            if action[0] == "R":
                if roll[0] == roll[1]: jailed = False
            elif action[0] == "P":
                self.state.debt[player] += 50
                jailed = False
            elif action[0] == "C":
                card = action[1]
                if card == CHANCE_GOJF_PROPERTY or card == COMMUNITY_GOJF_PROPERTY:
                    prop = self.state.properties[card]
                    if prop.owner == player:
                        prop.owner = -1
                        if card == CHANCE_GOJF_PROPERTY:
                            self.state.chanceCards.append(CHANCE_GOJF_CARD)
                        elif card == COMMUNITY_GOJF_PROPERTY:
                            self.state.communityCards.append(COMMUNITY_GOJF_CARD)
                        jailed = False
            if jailed and self.state.jailTurns[player] == 3:
                self.state.debt[player] += 50
                jailed = False
            if jailed:
                return
            else:
                self.state.positions[player] = 10
        # out of jail
        self.state.jailTurns[player] = 0
        self.state.phase = Phase.ROLL
        self.state.phaseData = roll
        self.__commitState()
        if roll[0] == roll[1]:
            self.state.numDoubles += 1
            if self.state.numDoubles == 3:
                self.state.positions[player] = -1
                self.state.numDoubles = 0
                self.state.turn += 1
                return
        totalRoll = roll[0] + roll[1]
        self.__travelSpaces(player, totalRoll)
        self.__runBSMT()
        if roll[0] != roll[1] or self.state.positions[player] == -1:
            self.state.numDoubles = 0
            self.state.turn += 1

    def __runBoardEffect(self, player, totalRoll):
        otherPlayer = (player + 1) % 2
        pos = self.state.positions[player]
        prop = self.state.properties[pos]
        type = prop.data.type
        if type == Type.PROPERTY or type == Type.RAILROAD or type == Type.UTILITY:
            if prop.owner == -1:
                self.state.phase = Phase.BUY
                self.state.phaseData = prop.id
                self.__commitState()
                willBuy = self.__runPlayer(player)
                # run BSMT to get money to buy with
                self.__runBSMT()
                # can't allow debt here, as the player would be able to mortgage the bought property to pay it off
                if willBuy and self.state.money[player] > prop.data.price:
                    self.state.money[player] -= prop.data.price
                    prop.owner = player
                else:
                    self.state.phase = Phase.AUCTION
                    self.state.phaseData = prop.id
                    self.__commitState()
                    bids = [min(self.__runPlayer(p), self.state.money[p]) for p in (player, otherPlayer)]
                    winner = player if bids[0] > bids[1] else otherPlayer
                    self.state.money[winner] -= max(bids)
                    prop.owner = winner
            elif prop.owner == otherPlayer and not prop.mortgaged:
                if type == Type.PROPERTY:
                    if prop.houses == 0 and self.state.playerOwnsGroup(otherPlayer, prop.data.group):
                        self.state.makePayment(player, otherPlayer, prop.data.rents[0] * 2)
                    else:
                        self.state.makePayment(player, otherPlayer, prop.data.rents[prop.houses])
                elif type == Type.RAILROAD:
                    mult = 2 ** (self.state.getRailroadCount(otherPlayer) - 1)
                    if self.state.specialRent:
                        mult *= 2
                    self.state.makePayment(player, otherPlayer, mult * 25)
                elif type == Type.UTILITY:
                    mult = 10 if self.state.playerOwnsGroup(otherPlayer, Group.UTILITY) else 4
                    if self.state.specialRent:
                        mult = 10
                        roll = self.state.getNextRoll()
                        totalRoll = roll[0] + roll[1]
                    self.state.makePayment(player, otherPlayer, mult * totalRoll)
        elif type == Type.CHANCE:
            self.__runChanceCard(player)
        elif type == Type.COMMUNITY:
            self.__runCommunityCard(player)
        elif prop.data.name == "Go to Jail":
            self.state.positions[player] = -1
        else:
            self.state.makePayment(player, -1, prop.data.tax)

    def __runBSMT(self):
        self.state.phase = Phase.BSMT
        self.__commitState()
        actionMade = True
        trades = [0, 0]
        while actionMade:
            actionMade = False
            for player in self.players:
                action = self.__runPlayer(player)
                if action:
                    # limit to one trade per player
                    if action[0] == "T":
                        trades[player] += 1
                        if trades[player] > 1: continue
                    actionMade = True
                    self.__runBSMTAction(player, action)
        for player in self.players:
            self.state.money[player] -= self.state.debt[player]
            if self.state.money[player] < 0:
                self.state.winner = (player + 1) % 2
                raise GameOverException
        self.state.debt = [0, 0]

    def __runBSMTAction(self, player, action):
        # build / sell houses
        if action[0] == "B" or action[0] == "S":
            buying = action[0] == "B"
            newHouses = [prop.houses for prop in self.state.properties]
            cost = 0
            for id, houses in action[1]:
                prop = self.state.properties[id]
                if prop.data.type != Type.PROPERTY or not self.state.playerOwnsGroup(player, prop.data.group): return
                newHouses[id] += houses if buying else -houses
                cost += houses * prop.data.houseCost if buying else -houses * prop.data.houseCost / 2
            if self.state.money[player] < cost: return
            totalHouses = 0
            totalHotels = 0
            for houses in newHouses:
                if houses == 5: totalHotels += 1
                else: totalHouses += houses
            if totalHouses > MAX_HOUSES or totalHotels > MAX_HOTELS: return
            for id, houses in action[1]:
                prop = self.state.properties[id]
                if newHouses[id] < 0 or newHouses[id] > 5: return
                for groupProp in self.state.getGroupProperties(prop.data.group):
                    if groupProp.mortgaged or abs(newHouses[groupProp.id] - newHouses[prop.id]) > 1: return
            for id, houses in enumerate(newHouses):
                self.state.properties[id].houses = houses
            self.state.money[player] -= cost
            self.__commitState()
        # mortgage / unmortgage
        elif action[0] == "M":
            cost = 0
            for id in action[1]:
                prop = self.state.properties[id]
                if prop.owner != player: return
                if prop.mortgaged:
                    # pay to unmortgage
                    cost += prop.data.price / 2 * 1.1
                else:
                    # get money for mortgaging
                    cost -= prop.data.price / 2
                    for groupProp in self.state.getGroupProperties(prop.data.group):
                        if groupProp.houses > 0: return
            if self.state.money[player] < cost: return
            for id in action[1]:
                prop = self.state.properties[id]
                prop.mortgaged = not prop.mortgaged
            self.state.money[player] -= cost
            self.__commitState()
        # trade
        elif action[0] == "T":
            tradeData = TradeData(action[1], tuple(action[2]), action[3], tuple(action[4]))
            otherPlayer = (player + 1) % 2
            if self.state.money[player] < tradeData.moneyOffered or self.state.money[otherPlayer] < tradeData.moneyRequested: return
            propsOffered = [self.state.properties[x] for x in tradeData.propertiesOffered]
            propsRequested = [self.state.properties[x] for x in tradeData.propertiesRequested]
            for prop in propsOffered:
                if prop.owner != player: return
                for groupProp in self.state.getGroupProperties(prop.data.group):
                    if groupProp.houses > 0: return
            for prop in propsRequested:
                if prop.owner != otherPlayer: return
                for groupProp in self.state.getGroupProperties(prop.data.group):
                    if groupProp.houses > 0: return
            self.state.phase = Phase.TRADE
            self.state.phaseData = tradeData
            self.__commitState()
            accepted = self.__runPlayer(otherPlayer)
            if accepted:
                self.state.money[player] += tradeData.moneyRequested - tradeData.moneyOffered
                self.state.money[otherPlayer] += tradeData.moneyOffered - tradeData.moneyRequested
                for prop in propsOffered:
                    prop.owner = otherPlayer
                for prop in propsRequested:
                    prop.owner = player
            self.state.phase = Phase.BSMT
            self.__commitState()

    def __runChanceCard(self, player):
        card = self.state.chanceCards.pop(0)
        if card != CHANCE_GOJF_CARD: self.state.chanceCards.append(card)
        pos = self.state.positions[player]
        self.state.phase = Phase.CHANCE
        self.state.phaseData = card
        self.__commitState()
        if card == 0:
            self.__travelSpaces(player, 40 - pos)
        elif card == 1:
            self.__travelSpaces(player, (24 - pos) % 40)
        elif card == 2:
            self.__travelSpaces(player, (11 - pos) % 40)
        elif card == 3:
            self.state.specialRent = True
            self.__travelSpaces(player, min((12 - pos) % 40, (28 - pos) % 40))
            self.state.specialRent = False
        elif card == 4 or card == 5:
            self.state.specialRent = True
            self.__travelSpaces(player, min((5 - pos) % 40, (15 - pos) % 40, (25 - pos) % 40, (35 - pos) % 40))
            self.state.specialRent = False
        elif card == 6:
            self.state.makePayment(-1, player, 50)
        elif card == 7:
            self.state.properties[CHANCE_GOJF_PROPERTY].owner = player
        elif card == 8:
            self.__travelSpaces(player, -3)
        elif card == 9:
            self.state.positions[player] = -1
        elif card == 10:
            cost = 0
            for prop in self.state.properties:
                if prop.owner == player:
                    if prop.houses == 5: cost += 100
                    else: cost += 25 * prop.houses
            self.state.makePayment(player, -1, cost)
        elif card == 11:
            self.state.makePayment(player, -1, 15)
        elif card == 12:
            self.__travelSpaces(player, (5 - pos) % 40)
        elif card == 13:
            self.__travelSpaces(player, (39 - pos) % 40)
        elif card == 14:
            self.state.makePayment(player, (player + 1) % 2, 50)
        elif card == 15:
            self.state.makePayment(-1, player, 150)

    def __runCommunityCard(self, player):
        card = self.state.communityCards.pop(0)
        if card != COMMUNITY_GOJF_CARD: self.state.communityCards.append(card)
        pos = self.state.positions[player]
        self.state.phase = Phase.COMMUNITY
        self.state.phaseData = card
        self.__commitState()
        if card == 0:
            self.__travelSpaces(player, 40 - pos)
        elif card == 1:
            self.state.makePayment(-1, player, 200)
        elif card == 2:
            self.state.makePayment(player, -1, 50)
        elif card == 3:
            self.state.makePayment(-1, player, 50)
        elif card == 4:
            self.state.properties[COMMUNITY_GOJF_PROPERTY].owner = player
        elif card == 5:
            self.state.positions[player] = -1
        elif card == 6:
            self.state.makePayment((player + 1) % 2, player, 50)
        elif card == 7:
            self.state.makePayment(-1, player, 100)
        elif card == 8:
            self.state.makePayment(-1, player, 20)
        elif card == 9:
            self.state.makePayment((player + 1) % 2, player, 10)
        elif card == 10:
            self.state.makePayment(-1, player, 100)
        elif card == 11:
            self.state.makePayment(player, -1, 50)
        elif card == 12:
            self.state.makePayment(player, -1, 50)
        elif card == 13:
            self.state.makePayment(-1, player, 25)
        elif card == 14:
            cost = 0
            for prop in self.state.properties:
                if prop.owner == player:
                    if prop.houses == 5: cost += 115
                    else: cost += 40 * prop.houses
            self.state.makePayment(player, -1, cost)
        elif card == 15:
            self.state.makePayment(-1, player, 10)
        elif card == 16:
            self.state.makePayment(-1, player, 100)

    def __travelSpaces(self, player, spaces):
        oldPos = self.state.positions[player]
        newPos = oldPos + spaces
        if newPos >= 40:
            newPos %= 40
            self.state.money[player] += 200
        if newPos < 0:
            newPos %= 40
        self.state.positions[player] = newPos
        self.__runBoardEffect(player, spaces)

    def __decideWinner(self):
        values = self.state.money[:]
        for prop in self.state.properties:
            # ignore get out of jail free card
            if prop.id == CHANCE_GOJF_PROPERTY or prop.id == COMMUNITY_GOJF_PROPERTY: continue
            if prop.owner != -1:
                value = prop.data.price
                if prop.mortgaged: value /= 2
                value += prop.data.houseCost * prop.houses
                values[prop.owner] += value
        return 1 if values[0] >= values[1] else 2

    def __commitState(self):
        self.stateTuple = self.state.toTuple()
        self.state.pastStates.append(self.stateTuple)
        for player in self.players:
            self.agents[player].receiveState(self.stateTuple)
        if self.gui: self.gui.drawState(self.state)
