Look at example.py for how to use the adjudicator.

Other files:
- dummy.py: example agent
- board.py: board info and constants
- adjudicator.py: adjudicator implementation
- state.py: state implementation

We'd recommend against modifying these files,
as they may be changed somewhat frequently.

If you find any bugs or mistakes in the adjudicator,
please send an email to naum.marten@stonybrook.edu
so we can fix it.

Team Boardwalk

Naum Marten
Brian Kondracki