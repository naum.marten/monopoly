from .adjudicator import Adjudicator
from .dummy import Dummy

adjudicator = Adjudicator()
agent1 = Dummy(0)
agent2 = Dummy(1)

winner, state = adjudicator.runGame(agent1, agent2)

print(winner)
print(state)
