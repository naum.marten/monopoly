import random
from collections import namedtuple

from .board import board, MAX_HOUSES, MAX_HOTELS, groups, Group

StateTuple = namedtuple("StateTuple", "turn properties positions money phase phaseData debt pastStates")
TradeData = namedtuple("TradeData", "moneyOffered propertiesOffered moneyRequested propertiesRequested")

# makes it possible to print a state without printing all of the past states
class StateList(list):
    def __str__(self):
        return str(len(self)) + " past states"

    def __repr__(self):
        return str(len(self)) + " past states"

    def __deepcopy__(self, memo):
        return StateList(self)

class Property:
    def __init__(self, id, owner, houses, mortgaged):
        self.id = id
        self.data = board[id] if id < 40 else None
        # -1 is unowned, 0 is player 1, 1 is player 2
        self.owner = owner
        # 5 houses = hotel
        self.houses = houses
        self.mortgaged = mortgaged

    def toValue(self):
        if self.owner == -1: return 0
        mult = 1 - self.owner * 2
        if self.mortgaged: return mult * 7
        return mult * (self.houses + 1)

    # speed up deepcopy of state
    def __deepcopy__(self, memo):
        return Property(self.id, self.owner, self.houses, self.mortgaged)

class State:
    def __init__(self, stateTuple=None):
        if stateTuple:
            self.turn = stateTuple[0]
            self.properties = []
            for id, value in enumerate(stateTuple[1]):
                owner = -1 if value == 0 else 0 if value > 0 else 1
                value = abs(value)
                houses = 0 if value == 0 or value == 7 else value - 1
                mortgaged = value == 7
                self.properties.append(Property(id, owner, houses, mortgaged))
            self.positions = stateTuple[2]
            self.money = stateTuple[3]
            self.phase = stateTuple[4]
            self.phaseData = stateTuple[5]
            self.debt = stateTuple[6]
            self.pastStates = stateTuple[7]
        else:
            self.turn = 0
            self.properties = [Property(i, -1, 0, False) for i in range(0, 42)]
            self.positions = [0, 0]
            self.money = [1500, 1500]
            self.phase = Phase.BSMT
            self.phaseData = None
            self.debt = [0, 0]
            self.pastStates = StateList()
            # extra state
            self.winner = -1
            self.numDoubles = 0
            self.jailTurns = [0, 0]
            self.diceRolls = None
            self.chanceCards = [i for i in range(0, 16)]
            self.communityCards = [i for i in range(0, 17)]
            random.shuffle(self.chanceCards)
            random.shuffle(self.communityCards)
            # used for some chance / community chest cards
            self.specialRent = False

    def getNextRoll(self):
        if self.diceRolls:
            return self.diceRolls.pop(0)
        if self.diceRolls == []:
            raise GameOverException
        return random.randint(1, 6), random.randint(1, 6)

    def getHousesRemaining(self):
        houses = MAX_HOUSES
        for prop in self.properties:
            if prop.houses < 5: houses -= prop.houses
        return houses

    def getHotelsRemaining(self):
        hotels = MAX_HOTELS
        for prop in self.properties:
            if prop.houses == 5: hotels -= 1
        return hotels

    def getGroupProperties(self, group):
        return [self.properties[id] for id in groups[group]]

    def getOwnedProperties(self, player):
        return [prop for prop in self.properties if prop.owner == player and prop.id < 40]

    def getOwnedGroupProperties(self, player):
        owned = self.getOwnedProperties(player)
        return [prop for prop in owned if self.playerOwnsGroup(player, prop.data.group)]

    def getOwnedGroups(self, player):
        return [group for group in range(0, 10) if self.playerOwnsGroup(player, group)]

    def getOwnedBuildableGroups(self, player):
        return [group for group in range(0, 8) if self.playerOwnsGroup(player, group)]

    def playerOwnsGroup(self, player, group):
        for prop in self.getGroupProperties(group):
            if not prop.owner == player: return False
        return True

    def getRailroadCount(self, player):
        count = 0
        for prop in self.getGroupProperties(Group.RAILROAD):
            if prop.owner == player: count += 1
        return count

    def makePayment(self, player1, player2, amount):
        if player1 != -1: self.debt[player1] += amount
        if player2 != -1: self.money[player2] += amount

    def toTuple(self):
        return StateTuple(self.turn, tuple([prop.toValue() for prop in self.properties]), tuple(self.positions),
                          tuple(self.money), self.phase, self.phaseData, tuple(self.debt), StateList(self.pastStates))

    def __str__(self):
        return str(self.toTuple())

class Phase:
    BSMT = 0
    TRADE = 1
    ROLL = 2
    BUY = 3
    AUCTION = 4
    RENT = 5
    JAIL = 6
    CHANCE = 7
    COMMUNITY = 8

class GameOverException(Exception):
    pass
