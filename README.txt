All Python files in the main directory are from the final adjudicator.
The "adj" directory contains our adjudicator implementation (including GUI).
The "agents" directory contains our agents, including baselines and our most advanced agent.
The "run" directory contains all of our other code.

Our adjudicator and GUI were created with simplicity in mind and have
no requirements other than Tkinter, which should come with Python.

Our GUI was made to work with any adjudicator, and simply running "run/newgui.py"
will show a game where our best agent plays against itself (using the final adjudicator).

Our best agent is in "agents/riskyagent.py".

- Team Boardwalk