import random

from adj.state import State

class RandomAgent:
    def __init__(self, id):
        self.id = id
        self.pid = id - 1

    def getBSMTDecision(self, state):
        state = State(state)

        ownedProperties = self.getOwnedProperties(state)
        if len(ownedProperties) == 0:
            return None
        randomProperty = ownedProperties[random.randint(0, len(ownedProperties) - 1)]
        # randomProperty = state.properties[random.randint(0,39)].id

        action = random.randint(0, 2)

        if action == 0:
            return "B", [(randomProperty.id, 1)]
        elif action == 1:
            return "S", [(randomProperty.id, 1)]
        else:
            return None

    # Always accept trades
    def respondTrade(self, state):
        return random.randint(0, 1)

    # Always return true, let the adjudicator decide if this is valid
    def buyProperty(self, state):
        return random.randint(0, 1)

    # Never attempt to win an auction
    def auctionProperty(self, state):
        state = State(state)
        if random.randint(0, 1):
            return random.randint(0, state.money[self.pid])
        else:
            return 0

    # Always buy out of jail if there's enough money
    def jailDecision(self, state):
        if random.randint(0, 1):
            return "R"
        else:
            return "P"

    def receiveState(self, state):
        pass

    def getOwnedProperties(self, state):
        return [prop for prop in state.properties if prop.owner == self.pid and prop.id < 40]

    def getOwnedGroupProperties(self, state):
        owned = self.getOwnedProperties(state)
        return [prop for prop in owned if state.playerOwnsGroup(self.pid, prop.data.group)]
