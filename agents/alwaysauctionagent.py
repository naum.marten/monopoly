import random

class AlwaysAuctionAgent:
    def __init__(self, id):
        self.id = id

    def getBSMTDecision(self, state):
        return None

    def respondTrade(self, state):
        return False

    def buyProperty(self, state):
        return False

    def auctionProperty(self, state):
        return random.randint(0, 10)

    def jailDecision(self, state):
        return ("R")

    def receiveState(self, state):
        self.state = state
