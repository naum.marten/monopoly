import random

from .properties import *

class FixedAgent:

    def __init__(self, id, maxThreshold, minThreshold):
        self.id = id
        self.pid = id - 1
        self.maxThreshold = maxThreshold
        self.minThreshold = minThreshold

    def getBSMTDecision(self, state):
        state = State(state)

        if random.randint(0, 1):
            return None

        if state.money[self.pid] > self.maxThreshold:
            ownedGroupProperties = getOwnedGroupProperties(self.pid, state)
            if len(ownedGroupProperties) == 0: return None
            randomProperty = ownedGroupProperties[random.randint(0, len(ownedGroupProperties) - 1)]
            return "B", [(randomProperty.id, 1)]
        elif state.money[self.pid] < self.minThreshold:
            ownedProperties = getOwnedProperties(self.pid, state)
            if len(ownedProperties) == 0:
                return None
            randomProperty = ownedProperties[random.randint(0, len(ownedProperties) - 1)]
            if randomProperty.mortgaged: return None
            return "M", [randomProperty.id]
        else:
            return None

    # Always accept trades
    def respondTrade(self, state):
        return random.randint(0, 1)

    # Always return true, let the adjudicator decide if this is valid
    def buyProperty(self, state):
        state = State(state)
        if state.money[self.pid] > self.maxThreshold:
            return True
        return False

    # Never attempt to win an auction
    def auctionProperty(self, state):
        state = State(state)
        prop = state.properties[state.phaseData[0]]
        if state.money[self.pid] > self.maxThreshold:
            return random.randint(0, prop.data.price)
        else:
            return 0

    # Always buy out of jail if there's enough money
    def jailDecision(self, state):
        state = State(state)
        if state.money[self.pid] > self.maxThreshold:
            return "P"
        return "R"

    def receiveState(self, state):
        pass
