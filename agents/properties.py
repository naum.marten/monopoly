from adj.board import *
from adj.state import State

def getPropertyColorGroup(position):
	group = board[position].group
	if(group != Group.NONE):
		return group
	return None

def getColorGroupProperties(colorGroup):
	return groups[colorGroup]

def getColorGroupOwners(colorGroup, state):
	properties = state.properties
	colorGroupProperties = getColorGroupProperties(colorGroup)
	if(colorGroupProperties != None):
		return [properties[prop] for prop in colorGroupProperties]

def getOwnedPropertiesInColorGroup(playerID, colorGroup, state):
	if(playerID == 1):
		return sum(prop > 0 for prop in getColorGroupOwners(colorGroup, state))
	else:
		return sum(prop < 0 for prop in getColorGroupOwners(colorGroup, state))

def getOwnedProperties(ID, state):
	return [prop for prop in state.properties if prop.owner == ID and prop.id < 40]

def getOwnedGroupProperties(ID, state):
	owned = getOwnedProperties(ID, state)
	return [prop for prop in owned if state.playerOwnsGroup(ID, prop.data.group)]

def getOwnedColorGroups(ID, state):
	return [colorGroup for colorGroup in range(0,10) if state.playerOwnsGroup(ID, colorGroup)]

def getNumHousesInColorGroup(colorGroup, state):
	colorGroupProperties = getColorGroupProperties(colorGroup)
	return [state.properties[prop].houses for prop in colorGroupProperties]
