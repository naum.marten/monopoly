class BaselineAgent:
    def __init__(self, id):
        self.id = id

    def getBSMTDecision(self, state):
        return None

    # Always accept trades
    def respondTrade(self, state):
        return False

    # Always return true, let the adjudicator decide if this is valid
    def buyProperty(self, state):
        return True

    # Never attempt to win an auction
    def auctionProperty(self, state):
        return 0

    # Always buy out of jail if there's enough money
    def jailDecision(self, state):
        return ("R")

    def receiveState(self, state):
        pass
