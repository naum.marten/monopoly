from .properties import *

class StrategicAgent:

    def __init__(self, id):
        self.id = id - 1
        self.opponentid = 1 if self.id == 0 else 0

    def getBSMTDecision(self, state):
        '''
        BUY: Purchase improvements to owned properties
		- Generally want to buy one house of each of the color groups asap
        '''
        state = State(state)

        ownedColorGroups = [group for group in getOwnedColorGroups(self.id, state) if group != Group.UTILITY and group != Group.RAILROAD
                            and abs(sum(getNumHousesInColorGroup(group, state))) < 4]

        if (len(ownedColorGroups) != 0):
            colorGroupCost = sum(board[prop].houseCost for prop in getColorGroupProperties(ownedColorGroups[0]))
            if (state.money[self.id] > colorGroupCost + 400):
                test = [(prop, 1) for prop in getColorGroupProperties(ownedColorGroups[0])]
                return "B", test

        '''
        SELL: Sell improvements to owned properties
		- Reserved for when the situation is dire mostly
		- Could possibly use this as a way to make money quick for immediate purchases (complete color group)
        '''

        '''
        MORTGAGE: Mortgage property back to the bank
		- Purchase as many properties as possible and then mortage them as needed in order to prevent other players from owning them
        '''

        '''
        TRADE: Propose trade to opponent
		- Can possibly use this to take advantage of unprepared bots
        '''
        return None

    def respondTrade(self, state):
        '''
        Generally don't want to accept trades as they will most likely favor the opponent considerably. However may wnat to be on the lookout for
        stupid trades that the opponent makes for some reason
        '''
        return False

    def buyProperty(self, state):
        '''
        Want to favor this towards the beginning of the game and taper off as time goes on
        '''
        state = State(state)
        position = state.positions[0]
        colorGroup = getPropertyColorGroup(position)

        # If this is a property that we can even buy
        if (colorGroup):
            # Prevent opponent from completing a color group
            # if(getOwnedPropertiesInColorGroup(self.opponentid, colorGroup, state) == len(getColorGroupProperties(colorGroup))-1):
            if (getOwnedPropertiesInColorGroup(self.opponentid, colorGroup, state) > 0 and getOwnedPropertiesInColorGroup(self.id, colorGroup, state) == 0):
                return True

            # If this is the beginning of the game, we want to purchase as much as we can
            if (state.turn < 10 and state.money[self.id] > board[position].price * 1.5):
                return True

            # If we are later in the game, we may want to purchase a property if we have a lot of money or if it is a property we value a lot
            if (state.money[self.id] > board[position].price * 7):
                return True

        return False

    def auctionProperty(self, state):
        '''
        Favor this over buying properties outright.
        The question is: How much to auction in order to ensure we will win but use the least amount of money possible?
        '''
        return board[state.phaseData].price * 0.5

    def jailDecision(self, state):
        '''
        At beginning of the game want to buy out of jail asap, but want to stay in jail longer towards the end
        '''
        if (state.turn < 20): return ("P")
        else: return ("R")

    def receiveState(self, state):
        pass
